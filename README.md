# osu!search download util

This is a small extension that basically proxies the beatmap download requests to `chrome.downloads.download` which is only available for extensions. This allows you to download multiple beatmaps at once.

This is required because it is not possible to trigger multiple beatmap downloads natively, as it is not possible to open new windows without user interaction, and the download endpoint does not allowing embedding to an iframe anymore.

## Chrome

[Chrome extension](https://chrome.google.com/webstore/detail/hkeaohmjpmbcfppbdcelkpkmeggghmlc)