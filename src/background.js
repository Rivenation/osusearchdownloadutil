chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  switch (message.type) {
    case "DOWNLOAD_BEATMAP":
      handleDownloadBeatmap(message, sendResponse);
      break;
  }
  return true;
});

const handleDownloadBeatmap = async (payload, sendResponse) => {
  const url = `https://osu.ppy.sh/beatmapsets/${payload.id}/download?noVideo=1`;
  const dlItemId = await startDownload(url);
  const dlItem = await findDownloadById(dlItemId);
  if (downloadSuccessful(dlItem)) {
    sendResponse({
      error: false,
      message: `Started downloading beatmapset ${payload.id}`
    });
  } else {
    sendResponse({
      error: true,
      message: `Failed to start download for beatmapset ${payload.id}. Are you logged in to the osu! site?`
    });
  }
};

const downloadSuccessful = dlItem => dlItem.finalUrl.startsWith("https://bm");

const startDownload = url => new Promise(resolve => chrome.downloads.download({ url }, resolve));

const findDownloadById = id =>
  new Promise(resolve => chrome.downloads.search({ id }, resolve)).then(
    ids => (ids.length > 0 ? ids[0] : undefined)
  );
