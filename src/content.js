window.onload = () => {
  log("[osu!search download utils] Loaded");
  document.body.setAttribute("data-download-utils", "true");
  observeBeatmapContainer(setUpButtonListener);
};

const WAIT_TIME = 3000;

let listener = null;
const setUpButtonListener = () => {
  if (listener) return;
  const button = document.querySelector("#dl-all-btn");
  listener = () => {
    if (button.classList.contains("loading")) {
      log("Already downloading beatmaps, not queueing more");
      return;
    }
    if (button.getAttribute("data-disabled") === "true") {
      log("DL button is disabled, not downloading");
      return;
    }
    const ids = currentBeatmapsetIds();
    log(`Downloading ${ids.length} beatmaps`);
    downloadAll(ids);
    button.classList.add("loading");
    setTimeout(() => button.classList.remove("loading"), ids.length * WAIT_TIME);
  };
  button.addEventListener("click", listener);
};

const downloadAll = ids =>
  ids.forEach((id, idx) =>
    setTimeout(() => {
      download(id).catch(e => showError(e));
    }, idx * WAIT_TIME)
  );

const download = id =>
  new Promise((resolve, reject) =>
    chrome.runtime.sendMessage(null, { type: "DOWNLOAD_BEATMAP", id }, null, response => {
      if (!response) return reject(new Error("Unknown error while downloading beatmap"));
      if (response.error === false) {
        resolve();
      } else {
        reject(new Error(response.message));
      }
    })
  );

const currentBeatmapsetIds = () => {
  const beatmapElements = Array.from(document.querySelectorAll("#beatmap-list > div"));
  return beatmapElements.map(e => e.getAttribute("data-id")).filter(id => id != null);
};

const observeBeatmapContainer = onChange => {
  const container = document.querySelector("#beatmap-container");
  const observer = new MutationObserver(onChange);
  observer.observe(container, { childList: true });
};

const showError = message => {
  console.error(message);
  const element = document.createElement("div");
  element.className = "ui message red";
  element.innerText = `Error downloading beatmap: ${message}`;
  const notificationContainer = document.querySelector("#notification-container");
  notificationContainer.appendChild(element);
};

const log = message => console.log(`[osu!search download utils] ${message}`);
